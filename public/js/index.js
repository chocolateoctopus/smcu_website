var serviceUrl = "/meetup";
var meetupData = {};

var fetchDataFromMeetup = function( callback ){
	$.get( serviceUrl, function( data ){
		console.log( data );
		meetupData = data;
		if( callback ) callback();
	});
};

$(document).ready(function(){
	fetchDataFromMeetup(function(){
		console.log('done fetching');
	});
});