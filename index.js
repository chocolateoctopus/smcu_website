var express = require('express');
var app = express();
var request = require('request');

app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));

app.get('/', function( req, res ){

	request('https://api.meetup.com/Saturday-Morning-Coding-Utrecht?photo-host=public&page=20&sig_id=140020322&sig=61c620f5fe4ce5dc0ff2a76e4493982e05cd7763', function( err, response, body ){
		if( !err && response.statusCode === 200 ){
			data = JSON.parse( body );
			// res.json( data );
			res.render('index', data );
		}
	});
});


app.get('/meetup', function( req, res ){
	request('https://api.meetup.com/Saturday-Morning-Coding-Utrecht?photo-host=public&page=20&sig_id=140020322&sig=61c620f5fe4ce5dc0ff2a76e4493982e05cd7763', function( err, response, body ){
		if( !err && response.statusCode === 200 ){
			data = JSON.parse( body );
			res.json( data );
		}
	});
});

app.listen( 1337 );
console.log('yoyo, I\'m on 1337. Bitch! ');