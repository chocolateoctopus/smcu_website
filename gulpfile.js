var gulp = require('gulp');
var less = require('gulp-less');
var path = require('path');
var nodemon = require('gulp-nodemon');

gulp.task('server', function(){
	nodemon({
		script: 'index.js',
		ext: 'js',
		env: { 'NODE_ENV': 'development' }
	});
});

gulp.task('less', function(){
  return gulp.src('./less/**/*.less')
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    .pipe(gulp.dest('./public/css'));
});

gulp.task('watch', function(){
  gulp.watch('./less/*.less', ['less']);
});

gulp.task('default', ['server', 'less', 'watch']);