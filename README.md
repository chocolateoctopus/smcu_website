## Website

This is the all mighty repo for the wonderfull website of the Saturday Morning Code Utrecht.

## Development

To start with contributing to the project you must first clone the repo.

	git clone https://<account-name>@bitbucket.org/chocolateoctopus/smcu_website.git

Make sure you have `gulp` installed on your system globally.

        npm install -g gulp

Then `cd` into the directory and run:

        npm install
 

Then to start the server and watch all the project files.

        gulp